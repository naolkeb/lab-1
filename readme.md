# Lab 1: Introduction

## Learning goals for this lab

1. Introduce yourself to the Python language, the use of Jupyter notebooks, and the use of Git repositories.
2. Learn how to make and manipulate numerical data as vectors and n-dimensional matrices.
3. Learn how to make, manipulate, and annotate figures.
4. Learn how to read in and manipulate tabular data.
5. Learn how to define functions and to use simple loops in your programming.
6. Learn how to read in and manipulate astronomical image data.

It is assumed that you have a basic knowledge of the command line.  (If you don't, read [this quick tutorial](cli.md) first and then come back here.)

# Introduction to Git and BitBucket

## Opening chat

**tl;dr:**  If you're using a PC, skip to the "Installing Git" section.  If you are using a Mac, skip to the "Setting up Git" section.

Imagine that you've just spent the last few weeks working on a critical programming assignment that is due tomorrow and suddenly, your laptop dies!  Fortunately, you made a few backups of your code to a flash drive, but then you realize that the most recent version you have backed up is two days old, and it contains several show-stopping bugs.  Since then, you had made radical revisions to the code (mostly fueled by adrenaline and unhealthy amounts of caffeine) that fixed many of the problems.  But that's ok, you remember all those changes, right?...

Imagine that you are working on a coding project with a few teammates, and everyone has a specific task to program and document.  Each time someone makes a change to the code, everyone should have a copy so that nobody is wasting time modifying old scrapped code, and so that nobody has to "reinvent the wheel" writing code someone else already completed.  At first, you decide that you will start a common Google Drive folder, to keep everyone on the same page.  However, one of your teammates starts to work on their portion of the project one evening and then has to leave to go somewhere, leaving broken, half finished code saved in the Google Drive.  Now, you are unable to test your portion until they get back and finish what they started.  There must be a better way to work on collaborative projects....

Imagine you are modifying a library you wrote several months ago and you come across a bizarre block of code.  You vaguely remember writing it, but you have no idea how it was supposed to work.  Of course, several-months-ago you thought the subroutine was obvious and didn't bother adding any comments, so now you're stuck spending the next hour investigating every single line.  If only there was a way you could ask your past self what you were thinking....

Imagine you are working on a large simulation for a final project.  As you begin writing the code, you have several ideas for how to implement different aspects of the simulation, but you don't know which ones will actually work.  So you copy-paste your folder containing your base code into a new temporary "testing" directory.  Before you know it, you have 50 folders with helpful names like "please\_work\_2" and "this\_one\_4" and "garbage\_6".  It becomes a nightmare to merge code different pieces of code that work well in the different directories.  And which folder contained the working version of feature X?...

### Why Git?

Git is a distributed *revision control system*; it is designed to help with the problems listed above (and many more).  But what does that mean?

Git keeps track of changes you make to your project, logging who made which changes and when.  If you ever need to reset everything in a project back to the way it was, that is easy to do in Git.  If you want to see when particular lines were modified in a file, Git can tell you that too.

Git makes it easy to *fork* your project when you want to experiment with or add something something new.  Let's say you are diligently working on programming feature X in your code and you suddenly have a great idea for a feature Y, but you're not sure you know completely how to implement it at the moment, or do not have the time to do so.  In Git, you can create a new *branch* in your project (that is, a clone of your project at this point in time) to start working on feature Y.  Maybe after a little while you get stuck, leaving your code in an unusable state.  But since you made a branch, you can go back to where you left off previously and continue working on feature X.  Later, you realize how to fix feature Y, so you switch to its branch and complete it.  Git then provides the tools to *merge* the working code in the feature Y branch back into the version of the project where you were working on feature X.

Git is designed to be distributed.  Let's say you and I decide to work on a project together. We set up a "master" repository of the project on a server, and then we each make a copy of it on our local computers.  If I make a change I'm happy with to a particular section of the project, I can *push* those changes to the remote server.  You, working on another part of the project, need the new changes I just created, so you *pull* the updated version from the server and *merge* your work with mine.  After a few more modifications, you are happy with your section of the project, so you *push* it back to the server.  If we want to add a third teammate, all they have to do is *clone* the copy of the project on the server to their local computer.  Then they can make their own edits and *push* them back to the server.

Note that the repository on the server can also have multiple branches, so you can push features you are currently working on to a development branch, collaborate with your teammates on the new feature, and then merge it with the master branch on the server when it is ready.

If at any point someone's computer dies, their work (that they pushed) is backed up on the server, so all they have to do is find another computer, clone the repository, and they're back to work!  If the server happens to die, everyone has a copy of the repository on their local machines, so a new master repository can be set up on another server.

In summary, Git is a program that keeps snapshots of the version history of files within a directory and allows you to share and collaborate on changes to that directory with multiple people on multiple computers.  Git is not limited to programming projects; it can be used to track any types of files in any kinds of projects.  However, many of it's features are designed to be particularly useful for tracking plain text files.

While other *revision control systems* exist and are in use (such as Mercurial, Subversion, TFVC, etc.), Git has far surpassed the others in terms of adoption.  Many services offering repository hosting, issue tracking, and documentation are available for Git projects (such as GitHub, BitBucket, and GitLab), further boosting it's popularity.  Odds are, if you join a collaborative project using revision control, they are probably already using Git.

## Installing Git

If your computer is running macOS or Linux, you probably already have Git installed.  To check this, open a terminal window and type in:

~~~
git --version
~~~

If you don't get an error, congrats, Git is installed on your system!  If you see an error message, you will need to install Git manually.

The Git executable files for all operating systems can be downloaded at [git-scm.com/downloads](https://git-scm.com/downloads).

If you are running the Windows installer, make a note of the "Adjusting your PATH environment" page.  Select "Use Git from the Windows Command Prompt" to allow you to use the access Git commands from PowerShell.  Otherwise, the default options should all be fine.

![](assets/path.png)

## Setting up Git

Git keeps track of who makes changes to files and when.  However, in order for Git to do that, it needs to know who you are!  The first time you use Git on a particular computer, you should run the following commands:

~~~
git config --global user.name "Firstname Lastname"
git config --global user.email "username@domain.ext"
~~~

replacing the text in quotes with your information.  For example:

![](assets/git_config.png)

## Git basics

Okay, so we are through with all of the introductions and configurations.  Whew!  Time to start actually using Git.

Let's create a new directory in the `Documents` folder and tell Git to start tracking it.  Open a fresh terminal window and enter the following commands.

~~~
cd Documents
mkdir git_tutorial
cd git_tutorial
git init
~~~

That last command created a hidden folder called `.git` inside the `git_tutorial` directory.  This hidden folder is where all the changes you make are stored as well as all the other metadata Git needs to track your project.

Git keeps track of changes in a directory through the use of "checkpoints" called commits.  Whenever you add new documents, modify existing documents, or remove items from a directory, you should commit those changes so that you can recover the directory at this point in history at a later time in the future.

Git allows you to choose which changes to add with each commit.  For example, if you changed something in file A and created a new file B, you can commit those changes together at once or separate them into two separate commits.  In general, it is good to commit one (and only one) *unit* of work at a time.  What that means is highly dependent on the circumstances - it can be a new function, it can be a new section in a document plus accompanying figures, it can be a whole file, etc.  However, everything in a commit should be related to a single clear change to your project.  (That is, a commit should **not** be "Fixed typos in abstract, changed title of 'Results' section to 'Discussion', and added a photo to the 'Methods' section."  Since there are 3 units of work in this example, it should be split up into 3 separate commits.  While this may at first seem overly fussy, it will make project management much easier on you and your collaborators in the future.  It is also extremely helpful when you try to use more advanced Git features, like `cherrypick` and `rebase`.)

Each commit requires you to write a message to go along with it.  The message for each commit in a directory is then stored in a log, allowing you to track your progress as you work on a project.  These messages should be a concise summary of the changes in their respective commits (no more than ~50 characters).  You also have the option to include a longer more detailed description after the summary if one is needed.

Let's make our first commit.  In general, it is a good practice for the first commit in each project to be empty.  In your terminal (continuing on from the previous section), enter in

~~~
git commit --allow-empty -m"Initial commit"
~~~

Let's break that down.  The `--allow-empty` flag tells git that you want to make a commit without any changes to the directory. The `-m` followed by text in quotes sets the commit message.  Only use the `-m` flag if you are not including an optional long description.

Next, let's add a file to the directory.  Create a new text document with the text "Hello, World!" (or whatever you want) and save it to the `git_tutorial` folder with the name `hi.txt`.

We can now ask Git if anything has changed with the command

~~~
git status
~~~

Git finds our new file in the directory.  However, right now it is untracked, meaning Git will not check for changes to this file unless we add it.  Let's do that now

~~~
git add hi.txt
~~~

Now, check the `git status` of our project.  Notice that the text for our `hi.txt` file has changed color from red to green.  Files in green are *staged* and files in red are *unstaged*.  Whenever we run the `git commit` command, Git will only add the changes in the staged files to the checkpoint.  This staging feature is to aid you in making atomic commits (that is, committing only one *unit* of work at a time).  You can add files to the stage with `git add <filename>` and remove them with `git reset HEAD <filename>`.  If you want to add all modified files to the stage quickly, use `git add .` (with the period).

![](assets/git_status.png)

We're ready to commit our `hi.txt` file.  Run the command

~~~
git commit
~~~

Git now launches a text editor for you to write your commit message.  Unless otherwise configured, the default editor Git chooses is VIM, a comand line editor.  VIM can be a little confusing to work with at first, but fortunately writing a simple message isn't that hard.

You'll notice that at first you cannot type.  This is because VIM is in command mode.  Press the letter I key to enter insert mode.

Type a short (~50 characters or less) title for your commit message summarizing the main change in this commit.  Then, press the return key ***twice*** and type a longer description.

You can think of commit messages as emails describing what a change does; the first line is the subject and the remaining lines are the body paragraphs of the email (in fact, you can have Git autogenerate emails from commit messages for you).  The blank line between the subject and the body is required for Git to separate the two.

When you are happy with your message, press the esc key and then type in `:wq`.  You should see these characters appear in the bottom left corner of your terminal.  Press enter and you will return to the command prompt.  An output will appear displaying the commit subject and what changed.

![](assets/git_commit_vim.png)

To review the commits made to your project so far, enter in the command

~~~
git log
~~~

You will see all the commits made to the project so far, when the commits were made, by whom, and the comitt messages.  Notice also that each commit has a random 40 hex-character hash associated with it.  This is called the commit SHA.  It is unique identifier generated for each commit you make.  As long as you know a commit's SHA, you can quickly find and reference it again.  In practice, you only really need to know the first 5 or 6 characters of a SHA and Git can figure out which commit you want to reference.

![](assets/git_log.png)

Let's make a change to our `hi.txt` file by adding another line of text.  If we save this file and then run a `git status`, we see that the file has indeed been modified since the last commit.  We can see the changes by running

~~~
git diff hi.txt
~~~

![](assets/git_diff.png)

The `git diff` command can be used to see changes to individual files or all files in the repository.  It can also compare between different commits.  For example, if one commit has a SHA starting with ab123 and another with cd456, we can see everything that changed between those commits with `git diff ab123..cd456`.

## BitBucket

BitBucket is an online service which can keep a copy of your Git repository and make it easy to share and collaborate with others.  BitBucket also provides many tools for teams to track issues, assign tasks, create and manage documentation, etc.  Other similar services exist (notably GitHub and GitLab), but we will be using BitBucket for this course since it allows you to keep unlimited private repositories for free with an academic account.

The instructors have created a Git repository on BitBucket for the class containing the lab materials.  You can access this repository by navigating to: [https://bitbucket.org/astro361fall2018/profile/projects](https://bitbucket.org/astro361fall2018/profile/projects)

The next thing for you to do is to register for an account on [BitBucket](https://bitbucket.org/).  **Make sure you register using your Michigan email address!**  Once you've made your account, send the instructors an email with your BitBucket username.

Now that you have your account set up, let's make a new repository with the directory we created above (you can easily delete it later under settings).  On the leftmost sidebar of the BitBucket site, click the "+" button and select repository.  Give it a name and set the access settings as desired.  *Make sure the repository type is set to Git*.  Then click the "Create repository" button.

In the upper right corner on the following webpage, there is an https text field (not the URL in your address bar).  Copy the URL you see there.  Then, go back to your terminal window and type in

~~~
git remote add origin <URL>
~~~

where `<URL>` is replaced with the one you copied from the website.  This command links your local repository to the remote one.  ***Note:*** *The paste shortcut in Terminal is sometimes different from that in other programs, so you may want to use the Edit menu.*

To sync your local changes with the remote repository, run

~~~
git push -u origin master
~~~

The first time you do this, you will be prompted for your BitBucket username and password.  (If it doesn't work, try using your UMich email address as the username.)

![](assets/git_push.png)

If you are collaborating or using multiple computers for a project, you can get new edits not yet stored on your local machine from the server using

~~~
git pull
~~~

## Closing remarks on Git

While the above barely scratches the surface of what Git can do, (and we haven't even gotten into some of the main features like branching and merging,) it will be sufficient for our purposes.  If you have the time and the desire, I highly reccommend checking out the [Lynda.com](https://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html) course for a more thorough overview.  You can access it for free by logging in through the "your organization portal" and connecting with your UMich account.

You can also view the help documentation for any Git command offline using

~~~
git help <command-name>
~~~

(use your arrow keys or PgUp/PgDn down to scroll, and press Q to quit)
